## Buscador de peliculas con Reactjs.

# Pasos para instalar en un ambiente local

## Instrucciones

- Clonar el repositorio

```
git clone https://gitlab.com/javier.mesias.everis/movie-search.git
```

- Entrar al directorio movie-search y ejecutar el comando 

```
npm install
```

- Dentro del directorio movie-search ejecutar el comando npm start esto levantara la app en http://localhost:3000/

```
npm start
``` 

## Fin instalación local
